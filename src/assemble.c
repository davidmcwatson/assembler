
struct label {
    char* name;
    size_t address;
    size_t index;
    struct label* next;
};

// arrays of instructions to match to their type
char* r_type_instructions[] = { "add", "sub", "and", "or", "xor" };
char* i_type_instructions[] = { "addi", "subi", "andi", "ori", "xori" };
char* j_type_instructions[] = { "lw", "sw", "j", "bnez", "beqz" };
typedef enum instruction_type_enum { R_TYPE, I_TYPE, J_TYPE } instruction_t;

instruction_t get_type( char* instruction )
{
    // instruction groups in order of enumeration
    char** intstruction_types[] = { r_type_instructions,
                                    i_type_instructions,
                                    j_type_instructions };
    int group;
    for( group = 0; group < 3; group++ )
    {
        for( int i = 0; i < 5; i++ )
        {
            if( strcmp( instruction, intstruction_types[ group ][ i ] ) == 0 )
            {
                return group;
            }
        }
    }
    return -1;
}

/*
 *  If a well formed label is found where line points, function will
 *  return with newly allocated null terminated string containing the label.
 *    Otherwise will return NULL.
 *  NOTE: returned pointer must be freed by calling code.
 */
char* extract_label( char* line )
{
    char* pos = line;
    char* out;
    while( !(*pos == ' ' || *pos == '\n' || *pos == ':') )
    {
        pos++;
    }

    if( *pos != ':' )
    {
        return NULL;
    }

    out = (char*)malloc( sizeof(char) * ((pos - line) + 1) );
    memcpy(out, line, (pos - line));
    out[pos - line] = 0;
    return out;
}

struct label* find_labels( char* source )
{
    char** lines = str_split( source, '\n' );
    char* label = NULL;
    struct label* root = NULL;

    size_t i = 0;
    size_t addr = 0;
    while(lines[i] != NULL)
    {
        if(*lines[i] == '#')
        {
            i++;
            continue;
        }
        label = extract_label(lines[i]);
        if(label)
        {
            struct label* tmp = root;
            root = (struct label*)malloc( sizeof(struct label) );
            root->name = label;
            root->address = addr;
            root->index = i;
            root->next = tmp;
            i++;
    #ifdef DEBUG
                printf("Label found on line: %lu address: 0x%#010zx: %s\n", i+i, addr, label);
    #endif

            continue;
        }
        addr++;
        i++;
    }
    free( lines );
    return root;
}

int is_label( size_t index, struct label* root )
{
    while(root != NULL)
    {
        if(root->index == index)
        {
            return 1;
        }
        root = root->next;
    }
    return 0;
}

void free_labels( struct label* root )
{
    while(root != NULL)
    {
        struct label* tmp = root->next;
        free( root->name );
        free( root );
        root = tmp;
    }
}

// register mapping
int resolve_register_symbol(char* symbol, registerMapping_t* symbolmap, uint8_t* out )
{
    while( symbolmap->name ) // symbol maps are null terminated
    {
        if( strncmp( symbol, symbolmap->name, strlen(symbol) ) == 0 )
        {
            *out = symbolmap->machinebyte;
            return 0;
        }
        symbolmap++;
    }
    return 1;
}

// instruction mapping
int resolve_instruction_symbol(char* symbol, instructionMapping_t* symbolmap, uint32_t* out )
{
    while( symbolmap->name ) // symbol maps are null terminated
    {
        if( strncmp( symbol, symbolmap->name, strlen(symbolmap->name) ) == 0 )
        {
            *out = symbolmap->machineword;
            return 0;
        }
        symbolmap++;
    }
    return 1;
}

// 32-bit constant resolution
int resolve_constant(char* symbol, uint32_t* out)
{
    int is_negative = 0;
    char* end; // later used by strtol();
    int base = 0;
    errno = 0;

    if(*symbol == '-') // check for negative sign
    {
        symbol++;
        is_negative = 1;
    }

    if(*symbol == '0')
    {
        symbol++;
        if(symbol) // lets look for a modifier
        {
            switch(*symbol)
            {
                case 'b': // ooh binary
                case 'B':
                    base = 2;
                    symbol++;
                    break;
                case 'x': // hexadecimal, we got this
                case 'X':
                    base = 16;
                    symbol++;
                    break;
                default:  // weird... lets let strtol deal with it
                    symbol -= 1; // negate pointer effects of looking for modifiers
                    break;
            }
        }
        else  // I guess it was a zero by itself ?
        {
             *out = 0;
             return 0;
        }
    }
    else // just plain old decimal
    {
        base = 10;
    }
    end = symbol;
    *out = (uint32_t)strtol(symbol, &end, base);
    if(end == symbol)
    {
#ifdef DEBUG
        printf("\tUnable to resolve: %s\n", symbol);
#endif
        return 1; // return error if unable to read
    }
    if(is_negative) // negate if need be
    {
        *out *= -1;
    }
    return 0; // otherwise allgood
}

// immediate resolution
int resolve_immediate_expression(char* symbol, uint16_t* out)
{
    uint32_t val = 0;
    int result = resolve_constant(symbol, &val);
    *out = (uint16_t) val;
    return result;
}

int resolve_offset_register_expression(char* symbol, uint8_t* regS, uint32_t* imm)
{
    char** parts = str_split_once( symbol, '(' );
    int result = 0;
    if(parts[1]) // check we have 2 parts
    {
      str_inplace_replace(parts[1], ')', 0 );
      result = resolve_constant(parts[0], imm );
      result &= resolve_register_symbol(parts[1], registerMap, regS );
      free( parts[1] );
    }
    else
    {
#ifdef DEBUG
      printf("\tUnable to resolve: %s\n", symbol);
#endif
      result = -1;
    }

    free( parts[0] );
    free( parts );

    return result;
}

int resolve_label_address( char* symbol, uint32_t* imm, struct label* labels, size_t curr_address )
{
    while(labels != NULL)
    {
        if( strncmp(symbol, labels->name, strlen(labels->name) )== 0 )
        {
            *imm = (labels->address - curr_address) - 1;
            return 0;
        }
        labels = labels->next;
    }
    return 1;
}

void print_error( char* line, int line_number )
{
    fprintf( stderr, "ERROR - Could not parse line number: %d: %s\n", line_number, line );
}

uint32_t resolve_r_type_instruction( char** symbols, int line_number )
{
    uint32_t out = 0;
    uint32_t regD = 0;
    uint32_t regS = 0;
    uint32_t regT = 0;

    char** registers = str_split( symbols[1], ',' );
    if( resolve_instruction_symbol( symbols[0], r_type_OPcode, &out ) )
    {
        fprintf( stderr, "ERROR - Could not parse instruction: %d: %s\n", line_number, symbols[0] );
        return 0;
    }
    if( resolve_register_symbol( registers[0], registerMap, (uint8_t*)&regD ) )
    {
        fprintf( stderr, "ERROR - Line %d: Could not parse register: %s\n", line_number, registers[0] );
        return 0;
    }
    if( resolve_register_symbol( registers[1], registerMap, (uint8_t*)&regS ) )
    {
        fprintf( stderr, "ERROR - Line %d: Could not parse register: %s\n", line_number, registers[1] );
        return 0;
    }
    if( resolve_register_symbol( registers[2], registerMap, (uint8_t*)&regT ) )
    {
        fprintf( stderr, "ERROR - Line %d: Could not parse register: %s\n", line_number, registers[2] );
        return 0;
    }
#ifdef DEBUG
    printf("\tAND: opcode: %#010x $d: %#04x $s: %#04x $t: %#04x\n", out, regD, regS, regT);
#endif
    regD <<= 24;
    regS <<= 20;
    out = out | regD | regS | regT;

    free( registers[0] );
    free( registers[1] );
    free( registers[2] );
    free( registers );
    return out;
}

uint32_t resolve_i_type_instruction( char** symbols, int line_number )
{
    uint32_t out = 0;
    uint32_t regD = 0;
    uint32_t regS = 0;
    uint32_t imm = 0;

    char** registers = str_split( symbols[1], ',' );
    if( resolve_instruction_symbol( symbols[0], i_type_OPcode, &out ) )
    {
        fprintf( stderr, "ERROR - Could not parse instruction: %d: %s\n", line_number, symbols[0] );
        return 0;
    }
    if( resolve_register_symbol( registers[0], registerMap, (uint8_t*)&regD ) )
    {
        fprintf( stderr, "ERROR - Line %d: Could not parse register: %s\n", line_number, registers[0] );
        return 0;
    }
    if( resolve_register_symbol( registers[1], registerMap, (uint8_t*)&regS ) )
    {
        fprintf( stderr, "ERROR - Line %d: Could not parse register: %s\n", line_number, registers[1] );
        return 0;
    }
    if( resolve_immediate_expression( registers[2], (uint16_t*)&imm ) )
    {
        fprintf( stderr, "ERROR - Line %d: Could not parse register: %s\n", line_number, registers[2] );
        return 0;
    }
#ifdef DEBUG
    printf("\tAND: opcode: %#010x $d: %#04x $s: %#04x imm: %#06x\n", out, regD, regS, imm);
#endif
    regD <<= 24;
    regS <<= 20;
    out = out | regD | regS | imm;

    free( registers[0] );
    free( registers[1] );
    free( registers[2] );
    free( registers );
    return out;
}

// TODO: implement this!!!
uint32_t resolve_j_type_instruction( char** symbols, int line_number, struct label* labels, size_t address )
{
    uint32_t out  = 0;
    uint32_t regD = 0;
    uint32_t regS = 0;
    uint32_t imm  = 0;

    if( resolve_instruction_symbol( symbols[0], j_type_OPcode, &out ) )
    {
        fprintf( stderr, "ERROR - Could not parse instruction: %d: %s\n", line_number, symbols[0] );
        return 0;
    }

    if( out == 0x40000000 ) // check for J instruction
    {
      if( resolve_label_address( symbols[1], &imm, labels, address ) )
      {
          if( resolve_constant( symbols[1], &imm ) )
          {
              fprintf( stderr, "ERROR - Line %d: Could not parse expression: %s\n", line_number, symbols[1] );
              return 0;
          }
      }
    }
    else
    {
        char** registers = str_split( symbols[1], ',' );
        if( out == 0xa0000000 || out == 0xb0000000 ) // Check for bnez or beqz
        {
            if( resolve_register_symbol( registers[0], registerMap, (uint8_t*)&regS ) )
            {
                fprintf( stderr, "ERROR - Line %d: Could not parse register: %s\n", line_number, registers[0] );
                return 0;
            }
            if( resolve_label_address( registers[1], &imm, labels, address ) )
            {
                if( resolve_constant( registers[1], &imm ) )
                {
                    fprintf( stderr, "ERROR - Line %d: Could not parse expression: %s\n", line_number, registers[1] );
                    return 0;
                }
            }
            free(registers[0]);
            free(registers[1]);
        }
        else  // fair to assume is lw or sw
        {
           if( resolve_register_symbol( registers[0], registerMap, (uint8_t*)&regD ) )
           {
               fprintf( stderr, "ERROR - Line %d: Could not parse register: %s\n", line_number, registers[0] );
               return 0;
           }
           if( resolve_offset_register_expression( registers[1], (uint8_t*)&regS, &imm ) )
           {
               fprintf( stderr, "ERROR - Line %d: Could not parse immediate offset register: %s\n", line_number, registers[0] );
               return 0;
           }
        }
        free( registers );
    }
#ifdef DEBUG
    printf("\tAND: opcode: %#010x $d: %#04x $s: %#04x imm: %#06x\n", out, regD, regS, imm);
#endif
    regD <<= 24;
    regS <<= 20;
    imm &= 0xfffff; // break down to 20-bits
    out = out | regD | regS | imm;
    return out;
}

void assemble(FILE* infile, FILE* outfile){
    char* source_code = read_file( infile );    // load in source code
    struct label* labels = find_labels( source_code );  // first pass to find all labels

    // second pass outputting all the bytes!
    char** lines = str_split( source_code, '\n' );
    char** symbols;
    char** beg = lines;
    uint32_t machineword = 0;

    int linenumber = 0;
    size_t address = 0;


    while(*lines != NULL)
    {
        linenumber++;
        if(**lines == '#') // ignore comments
        {
#ifdef DEBUG
            printf("Ignoring comment on line: %d: %s\n", linenumber, *lines);
#endif
            lines++;
            continue;
        }
        if( is_label( linenumber-1, labels) ) // ignore labels
        {
#ifdef DEBUG
            printf("Ignoring label on line: %d: %s\n", linenumber, *lines);
#endif
            lines++;
            continue;
        }
#ifdef DEBUG
        printf("line: %d in: %s\n", linenumber, *lines );
#endif
        // split line by spaces just once to get the instruction
        symbols = str_split_once( *lines, ' ' );

        if( strcmp(*symbols, ".word") == 0 )
        {
            // process word
            if( resolve_constant( symbols[1], &machineword ) )
            {
              print_error( *lines, linenumber );
              lines++;
              address++;
              free(symbols);
              continue;
            }
            fwrite((unsigned char*)&machineword, 4, 1, outfile);
        }
        else
        {
            instruction_t i_type = get_type( symbols[0] );
            switch ( i_type )
            {
                case R_TYPE:
                    machineword = resolve_r_type_instruction( symbols, linenumber );
                    break;
                case I_TYPE:
                    machineword = resolve_i_type_instruction( symbols, linenumber );
                    break;
                case J_TYPE:
                    machineword = resolve_j_type_instruction( symbols, linenumber, labels, address );
                    break;
                default:
                    print_error( *lines, linenumber );
                    lines++;
                    address++;
                    free(symbols);
                    continue;
            }
            fwrite((unsigned char*)&machineword, 4, 1, outfile);
        }
#ifdef DEBUG
        printf("\tout: %#010x\n", machineword);
#endif

        address++;
        free(*lines);
        free( symbols[0] );
        free( symbols[1] );
        free( symbols );
        lines++;
    }

    machineword = 0;
    fwrite((unsigned char*)&machineword, 4, 1, outfile);
    free_labels(labels);
    free(source_code);
    free(beg);
}
